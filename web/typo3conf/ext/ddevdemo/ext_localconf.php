<?php
defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['ddevdemo'] = 'EXT:ddevdemo/Configuration/RTE/Default.yaml';

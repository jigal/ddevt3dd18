<?php

/**
 * Extension Manager/Repository config file for ext "ddevdemo".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'ddevdemo',
    'description' => 'Sitepackage for the ddev Demo installation',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.5.99',
            'fluid_styled_content' => '8.7.0-9.5.99',
            'rte_ckeditor' => '8.7.0-9.5.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Jigal\\Ddevdemo\\' => 'Classes'
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Jigal van Hemert',
    'author_email' => 'jigal.van.hemert@typo3.org',
    'author_company' => 'Jigal',
    'version' => '1.0.0',
];
